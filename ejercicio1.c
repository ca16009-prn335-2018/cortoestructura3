#include <stdio.h>
#include <stdlib.h>

struct nodo{
	int info;
	struct nodo *sig;
};

struct nodo	*raiz = NULL;

void insertar(int x){
struct nodo *nuevo;
	nuevo = malloc(sizeof(struct nodo));
	nuevo->info = x;
	if(raiz	== NULL){
		raiz = nuevo;
		nuevo->sig == NULL;
	}
	else{
		nuevo->sig = raiz;
		raiz = nuevo;
	}
}
void imprimir(){
	struct nodo *reco=raiz;
	printf("Lista completa:\n");
	while(reco!=NULL){
		printf("%i ",reco->info);
		reco=reco->sig;
	}
	printf("\n");
}
int extraer(){
	if(raiz!=NULL){
		int informacion=raiz->info;
		struct nodo *bor=raiz;
		raiz=raiz->sig;
		free(bor);
		return informacion;
	}
	else{
		return -1;
	}
}
void liberar(){
	struct nodo *reco = raiz;
	struct nodo *bor;
	while(reco!=NULL){
		bor=reco;
		reco=reco->sig;
		free(bor);
	}
}

void estaVacia(){
	if(raiz == NULL){
		printf("La pila está vacía\n");
	}else{
		printf("La pila no está vacía\n");
	}
}
    
int cantidad() {
        int cant=0;
        struct nodo *reco=raiz;
        while (reco!=NULL) {
            cant++;
            reco=reco->sig;
        }
        return cant;
    }
int main(){
	estaVacia();
	insertar(10);
	insertar(40);
	insertar(3);
	imprimir();
	estaVacia();
	printf("Número de nodos: %i\n",cantidad());
	printf("Extraemos de la pila: %i\n",extraer());
	imprimir();
	liberar();
	printf("Número de nodos: %i\n",cantidad());
	return 0;
}
